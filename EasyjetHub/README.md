# EasyjetHub package folder structure

- `bin/`: Executables
  - `easyjet-ntupler`
  - `easyjet-gridsubmit`
  - `easyjet-test`
  - `metadata-check`
- `datasets/`: Text files holding dataset lists for grid submission
- `python/`: Python modules
- `share/`: Run configuration yaml files
- `src/`: C++ code
