#include "../BaselineVarsBoostedAlg.h"
#include "../BaselineVarsResolvedAlg.h"
#include "../JetPairingAlg.h"
#include "../TruthParticleInformationAlg.h"
#include "../JetBoostHistogramsAlg.h"

using namespace HH4B;

DECLARE_COMPONENT(BaselineVarsBoostedAlg)
DECLARE_COMPONENT(BaselineVarsResolvedAlg)
DECLARE_COMPONENT(JetPairingAlg)
DECLARE_COMPONENT(TruthParticleInformationAlg)
DECLARE_COMPONENT(JetBoostHistogramsAlg)
